import {html, LitElement} from 'lit-element';

class PersonaFichaListado extends LitElement{

    static get properties() {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object}

        }
    }

    render() {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div>
                <label>Nombre</label>
                <input type = "text" id= "fname" value = "${this.fname}">
                <br>
                <label>Años en la empresa</label>
                <input type = "text" id= "yearsInCompany" value = "${this.yearsInCompany}">
                <br>
                <img src = "${this.photo.src}" height = "200" width = "200" alt = "${this.photo.alt}">
                <br>
                <br>
            </div>
        `;
    }

} 

customElements.define('persona-ficha-listado', PersonaFichaListado)