import {html, LitElement} from 'lit-element';

class PersonaFooter extends LitElement{

    static get properties() {
        return {
        }
    }

    render() {
        return html`
            <h5>@PersonaFooter 2021</h5>
        `;
    }

} 

customElements.define('persona-footer', PersonaFooter)