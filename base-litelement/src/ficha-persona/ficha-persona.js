import {html, LitElement} from 'lit-element';

class FichaPersona extends LitElement{
    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},

        }
    }
    constructor(){
        super();
        this.name = "Prueba nombre"
        this.yearsInCompany = "12"
    }


    updated(changedProperties){
        console.log("update");
        changedProperties.forEach((oldValue,propName) => {
            console.log("Propiedad "+propName+" cambia valor anterior era "+ oldValue);

        });
        if (changedProperties.has("name")){
            console.log("Propiedad name ha cambiado de valor, anterior era "+changedProperties.get("name")+" nueva es "+this.name );
        }
        if (changedProperties.has("yearsInCompany")){
            this.udapteInfoCategory();
        }
    }

    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" name="name" value="${this.name}" @input=${this.updateName}> 
                <br>
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input=${this.updateYear}>
                <br>
                <input type="text" value="${this.personInfo}" disabled>

            </div>

        `;
    }
    
    updateName(e) {
        console.log("onChange")
        this.name = e.target.value
    }

    updateYear(e) {
        console.log("updateYear")
        this.yearsInCompany = e.target.value

    }

    udapteInfoCategory() {
        if(this.yearsInCompany >= 7){
            this.personInfo = "Master"
        }else if(this.yearsInCompany >= 5){
            this.personInfo = "Senior"
        }else if(this.yearsInCompany >= 3){
            this.personInfo = "Team"
        }else{
            this.personInfo = "Pakete"
        }
    }

} 

customElements.define('ficha-persona', FichaPersona)