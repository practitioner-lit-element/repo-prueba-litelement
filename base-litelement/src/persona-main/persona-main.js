import {html, LitElement} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado';

class PersonaMain extends LitElement{


    static get properties() {
        return {
            people: {type: Array}
        }
    }

    constructor() {
        super();
        this.people = [
            {
                name: "Leo Messi",
                yearsInCompany: 17,
                photo: {
                    src: "./img/messi.jpeg",
                    alt: "Leo Messi"
                }
            },
            {
                name: "Frankie de Jong",
                yearsInCompany: 2,
                photo: {
                    src: "./img/dejong.jpeg",
                    alt: "Frankie de Jong"
                }

            },
            {
                name: "Ansu Fati",
                yearsInCompany: 5,
                photo: {
                    src: "./img/ansufati.jpeg",
                    alt: "Ansu Fati"
                }

            },

            {
                name: "Gerard Pique",
                yearsInCompany: 5,
                photo: {
                    src: "./img/pique.jpeg",
                    alt: "Gerard Pique"
                }

            }
        ]
    }

    render() {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

            <h2 class="text-center">Personas</h2>
            <main>
                ${this.people.map(
                    person => html `<persona-ficha-listado 
                        fname = "${person.name}"
                        yearsInCompany = "${person.yearsInCompany}"
                        .photo = "${person.photo}"

                        >
                </persona-ficha-listado>`
                )}
                
            </main>
        `;
    }

} 

customElements.define('persona-main', PersonaMain)