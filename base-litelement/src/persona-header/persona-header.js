import {html, LitElement} from 'lit-element';

class PersonaHeader extends LitElement{

    static get properties() {
        return {
        }
    }

    render() {
        return html`
            <h1>PersonaHeader!</h1>
        `;
    }

} 

customElements.define('persona-header', PersonaHeader)